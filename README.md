This is the first part of practical demonstration for the Master thesis: "Application of neural networks in android apps"
This project consists of scripts necessary for training a neural network model with tensorflow for object detection, which will be used for android application in the second part of practical demonstration.

These scripts can be executed in many different environments. 
For "create dataset.py" and "prepare dataset.py" scripts, I used Windows. It's best to use virtual environment.
For "train model.py" script I used Miniconda environment on WSL2 on Windows.

You will need to install FiftyOne: https://docs.voxel51.com/getting_started/install.html 

I recommend using Python 3.8 because it is compatible with the tensorflow 2.8.0 which I used. You can find the compatibility table here: https://www.tensorflow.org/install/source

Here are step by step tutorials for installing WSL2 and Miniconda for Windows with NVIDIA:
- https://ubuntu.com/tutorials/install-ubuntu-on-wsl2-on-windows-11-with-gui-support
- https://ubuntu.com/tutorials/enabling-gpu-acceleration-on-ubuntu-on-wsl2-with-the-nvidia-cuda-platform#1-overview
- https://www.tensorflow.org/install/pip#windows-wsl2_1
- After all this you can run command pip install -r requirements.txt or follow instructions on https://colab.research.google.com/github/tensorflow/tensorflow/blob/master/tensorflow/lite/g3doc/models/modify/model_maker/object_detection.ipynb
- You might also need to execute conda install -c nvidia cuda-nvcc
- And you might need to increase maximum ram of your WSL environment if a limit has been set
