import numpy as np
import os

from tflite_model_maker.config import QuantizationConfig
from tflite_model_maker.config import ExportFormat
from tflite_model_maker import model_spec
from tflite_model_maker import object_detector

import tensorflow as tf
assert tf.__version__.startswith('2')

# efficientdet-lite[0-4]. Higher the number, better the accuracy, but greater the size
model_spec_name = "efficientdet-lite0"
batch_size = 8
epochs = 10

labels_path = "../../build/dataset/"
data_path = "/mnt/c/Users/filip/fiftyone/open-images-v7/"

classes = ""

tf.get_logger().setLevel('ERROR')
from absl import logging
logging.set_verbosity(logging.ERROR)

# get classes
classes_file = open("../assets/classes.txt", "r")
classes = classes_file.read().split('\n')
classes_file.close()

spec = object_detector.EfficientDetSpec(
    model_name=model_spec_name,
    uri='https://tfhub.dev/tensorflow/efficientdet/lite0/feature-vector/1',
    hparams={'max_instances_per_image': 8000}
)

print("\n\nLoading data...\n")

train_data = object_detector.DataLoader.from_pascal_voc(
    data_path + 'train/data',
    labels_path + 'train/labelsFixed',
    classes
)

validation_data = object_detector.DataLoader.from_pascal_voc(
    data_path + 'validation/data',
    labels_path + 'validation/labelsFixed',
    classes
)

test_data = object_detector.DataLoader.from_pascal_voc(
    data_path + 'test/data',
    labels_path + 'test/labelsFixed',
    classes
)

print("Creating model...\n")
model = object_detector.create(
    train_data, 
    model_spec=spec, 
    batch_size=batch_size, 
    train_whole_model=True, 
    epochs=epochs, 
    validation_data=validation_data)

print("\n\nEvaluating model...\n")
modelEvaluationResults = model.evaluate(test_data, batch_size)
print(modelEvaluationResults)

print("\nExporting model...\n")
model.export(export_dir='../../build/model', tflite_filename='object_detection.tflite')

print("\n\nEvaluating TFLite model...\n")
tfModelEvaluationResults = model.evaluate_tflite('../../build/model/object_detection.tflite', test_data)
print(tfModelEvaluationResults)