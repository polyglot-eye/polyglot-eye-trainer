import fiftyone as fo
import fiftyone.zoo as foz
from lxml import etree
import os
import shutil

dataset_path = "../../build/dataset/"
labels_folder = "labels"
labels_fixed_folder = "labelsFixed"

classes = ""
splits = ["validation", "test", "train"]

# get class names
classes_file = open("../assets/classes.txt", "r")
classes = classes_file.read().split('\n')
classes_file.close()

# prepare all the splits (train, validation and test)
for split in splits:

    print("\nPreparing " + split + "...")

    # create a new folder for prepared labels

    new_folder = dataset_path + split + "/" + labels_fixed_folder

    if (os.path.exists(new_folder)): 
        shutil.rmtree(new_folder)
    
    os.mkdir(new_folder)

    # all label files
    files = os.listdir(dataset_path + split + "/" + labels_folder)

    for file in files:

        # replace invalid characters in the file
        fullpath = dataset_path + split + "/" + labels_folder + "/" + file
        currentFile = open(fullpath, 'r')
        filedata = currentFile.read()
        currentFile.close()

        filedata = filedata.replace(' & ', ' &amp; ')

        currentFile = open(fullpath, 'w')
        currentFile.write(filedata)
        currentFile.close()

        # parse the file
        tree = etree.parse(dataset_path + split + "/" + labels_folder + "/" + file)
        root = tree.getroot()

        # remove unnecessary classes
        for child in root:
            if (child.tag == "object" and child.find("name").text not in classes):
                child.getparent().remove(child)

        # Add missing annotations
        for child in root:
            if (child.tag == "object"):
                if (child.find('difficult') is None):
                    difficultTag = etree.Element('difficult')
                    difficultTag.text = '0'
                    difficultTag.tail = child[0].tail
                    child.insert(1, difficultTag)

                if (child.find('truncated') is None):
                    truncatedTag = etree.Element('truncated')
                    truncatedTag.text = str(int(child.find('IsTruncated').text == 'True')) if child.find('IsTruncated') is not None else '0'
                    truncatedTag.tail = child[0].tail
                    child.insert(1, truncatedTag)

                if (child.find('pose') is None):
                    poseTag = etree.Element('pose')
                    poseTag.text = 'Unspecified'
                    poseTag.tail = child[0].tail
                    child.insert(1, poseTag)

        # write 
        tree.write(dataset_path + split + "/" + labels_fixed_folder + "/" + file, pretty_print=True)
