import fiftyone as fo
import fiftyone.zoo as foz
import shutil
import os

export_dir = "../../build/dataset/"
number_of_samples = 500
export_media = False

number_of_train_samples = int(number_of_samples * 0.8)
number_of_test_samples = int(number_of_samples * 0.1)
number_of_validation_samples = int(number_of_samples * 0.1)

classes = ""

# delete previous dataset
if os.path.isdir(export_dir):
    shutil.rmtree(export_dir)

# get class names
classes_file = open("../assets/classes.txt", "r")
classes = classes_file.read().split('\n')
classes_file.close()

print("\nLoading train dataset...\n")

trainDataset = foz.load_zoo_dataset(
    "open-images-v7",
    split="train",
    label_types=["detections"],
    classes=classes,
    max_samples=number_of_train_samples
)

trainDataset.export(
    export_dir=export_dir + "train",
    dataset_type=fo.types.VOCDetectionDataset,
    export_media=export_media,
    label_field="ground_truth",
)

print("\nFinished loading the train dataset.\n")

print("Loading test dataset...\n")

testDataset = foz.load_zoo_dataset(
    "open-images-v7",
    split="test",
    label_types=["detections"],
    classes=classes,
    max_samples=number_of_test_samples
)

testDataset.export(
    export_dir=export_dir + "test",
    dataset_type=fo.types.VOCDetectionDataset,
    export_media=export_media,
    label_field="ground_truth",
)

print("\nFinished loading the test dataset.\n")

print("Loading validation dataset...\n")

validationDataset = foz.load_zoo_dataset(
    "open-images-v7",
    split="validation",
    label_types=["detections"],
    classes=classes,
    max_samples=number_of_validation_samples
)

validationDataset.export(
    export_dir=export_dir + "validation",
    dataset_type=fo.types.VOCDetectionDataset,
    export_media=export_media,
    label_field="ground_truth",
)

print("\nFinished loading the validation dataset.\n")
